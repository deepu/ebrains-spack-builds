## Regular release of EBRAINS tools: Process to create new Spack installation root in case we want to update the Spack release without breaking the existing packages

1. In the Gitlab repository go to CI/CD > Variables > Expand and set the appropriate {site}_INSTALLATION_ROOT_{env} variable to the **new name**
2. In the Gitlab repository go to CI/CD > Variables > Expand and set the appropriate {site}_OPERATION_{env} variable to **"create"**
3. In the Gitlab repository go to CI/CD > Variables > Expand and set the appropriate {site}_LAB_KERNEL_PATH_{env} variable to the **desired path**
4. In the Gitlab repository go to CI/CD > Variables > Expand and set the appropriate {site}_SPACKIFIED_ENV_{env} variable to the **appropriate name**
5. Run the build pipeline
6. In the Gitlab repository go to CI/CD > Variables > Expand and set the appropriate {site}_OPERATION_{env} variable to **"manualFixing"** and run the build pipeline. This will create an OpenShift job that will not do any actual operation just loop forever, to give us the time to connect to the running pod and manually set the Spack instance to the commit we want for Spack to be. 
- We set the appropriate commit to the Spack instance
- Right after the previous action go to "spack/etc/spack" and create "packages.yaml" with content:
```
packages:
  all:
    target: [x86_64]
```
- Right after the previous action install also the appropriate python with `spack install python@3.8.11 %gcc@10.3.0`
- Then disconnect and kill the OpenShift job. 
7. In the Gitlab repository go to CI/CD > Variables > Expand and set the appropriate {site}_OPERATION_{env} variable to **"update"**
8. Run the build pipeline
