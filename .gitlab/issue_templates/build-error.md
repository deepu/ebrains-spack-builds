<!-- Thanks for taking the time to report this build failure. To proceed with the report please:

1. Title the issue "Installation issue: <name-of-the-package>".
2. Provide the information required below.

We encourage you to try, as much as possible, to reduce your problem to the minimal example that still reproduces the issue. That would help us a lot in fixing it quickly and effectively! -->

### Summary

|               |                                               |
|---------------|-----------------------------------------------|
| Summary       | Spack package build failure                   |
| Package info  | <!-- Spack package name or entire spec -->    |
| System        | <!-- HPC cluster name/Collab Lab/other  -->   |
| Related       | <!-- Other related issues (if applicable) --> |

### Steps to reproduce the issue

<!-- Fill in the console output from the exact spec you are trying to build. -->
```console
$ spack spec -I <spec>
...
$ spack install <spec>
...
```

### Error message
<!-- Please post the error message from spack inside the <details> tag below: -->

<details><summary>Error message</summary><pre><code>
(add error logs here)
</code></pre></details>

### Information on your system

<!-- Please include the output of `spack debug report` -->

<!-- If you have any relevant configuration detail (custom `packages.yaml` or `modules.yaml`, etc.) you can add that here as well. -->

### Additional information

<!-- Please upload the following files. They should be present in the stage directory of the failing build. Also upload any config.log or similar file if one exists. -->
* [spack-build-out.txt]()
* [spack-build-env.txt]()

/label ~build-error
