# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class BiobbAnalysis(PythonPackage):
    """Biobb_analysis is the Biobb module collection to perform analysis 
    of molecular dynamics simulations"""

    # Homepage and download url
    homepage = "https://github.com/bioexcel/biobb_analysis"
    git = 'https://github.com/bioexcel/biobb_analysis.git'
    url = 'https://github.com/bioexcel/biobb_analysis/archive/refs/tags/v4.0.1.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('4.0.1', sha256='3d7190d0cae6af42b2e7c0ecf073930a609b699ef311e8b74afc1634958e09b0')

    # Dependencies
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('biobb-common')
    depends_on('gromacs')
    depends_on('ambertools')

    # Patching to enable python 3.10 (not official, might not be stable)
    def patch(self):
        filter_file("    python_requires='>=3.7,<3.10',", "    python_requires='>=3.7,<3.11',", "setup.py")
        filter_file(
            "'Programming Language :: Python :: 3.9'",
            "'Programming Language :: Python :: 3.9',\r\n        "
            "'Programming Language :: Python :: 3.10'",
            "setup.py",
        )

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import biobb_analysis')
