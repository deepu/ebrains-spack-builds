# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySpalloc(PythonPackage):
    """Spalloc is a Python library and set of command-line programs for
    requesting SpiNNaker machines from a spalloc server."""

    homepage = "https://github.com/SpiNNakerManchester/spalloc"
    pypi = "spalloc/spalloc-1!7.0.0.tar.gz"

    version('7.0.0', sha256='e141a0e661efd6fd634f3793752d8d6deef56ee37a21fa8e3d7208f4edd86f51')

    depends_on("python@3.7:", type=("build", "run"))
    depends_on("py-jsonschema", type=("build", "run"))
    depends_on("py-spinnutilities@7.0.0", type=("build", "run"))
