# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class RUqsa(RPackage):
    """This package will solve an initial value problem (given an ordinary
    differential equation) for a list of simulation experiments. The
    simulations will be compared to data, repeatedly.  The package can
    sample the parameters of the model via ABC (approximate Bayesian
    computation) and by doing so quantify the uncertainty that remains
    within the model even after considering the data. Experiments are
    evaluated in a sequence, intermediate probability densities are
    modeled using the VineCopua package."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://github.com/icpm-kth/uqsa"
    url = "https://github.com/icpm-kth/uqsa/archive/refs/tags/v2.2.tar.gz"

    maintainers = ["akramer", "oliviaeriksson"]

    version("2.2", sha256="df15d20c0cd466083181ec8f37cd331f409dcd23838553cd46a7a7305bdcfc7f")

    depends_on("r-vinecopula")
    depends_on("r-mass")
    depends_on("r-r-utils")
    depends_on("r-ks")
    depends_on("r-gillespiessa2")
