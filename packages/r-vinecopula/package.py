# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class RVinecopula(RPackage):
    """Provides tools for the statistical analysis of regular vine copula 
    models, see Aas et al. (2009) <doi:10.1016/j.insmatheco.2007.02.001> and 
    Dissman et al. (2013) <doi:10.1016/j.csda.2012.08.010>.
    The package includes tools for parameter estimation, model selection,
    simulation, goodness-of-fit tests, and visualization. Tools for estimation,
    selection and exploratory data analysis of bivariate copula models are also
    provided."""

    homepage = "https://cran.r-project.org/package=VineCopula"
    url = "https://github.com/tnagler/VineCopula/archive/refs/tags/v2.4.5.tar.gz"

    version("2.4.5", sha256="2255a2a0f2644e447f33c7fa3562cb1ad109aa7809ea250fe82b67b35cc2d23d")

    depends_on("r-mass")
    depends_on("r-mvtnorm")
    depends_on("r-r-methodss3")
    depends_on("r-adgoftest")
    depends_on("r-lattice")
